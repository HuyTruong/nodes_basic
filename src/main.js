const yargs = require("yargs");
const { string, number } = require("yargs");
const chalk = require("chalk");
const { getListProduct, updateListProduct } = require("./service/product_service");

//get all Pruduct
yargs.command({
    command: "get-all-product",
    handler: () =>{
        let listProduct = getListProduct();
        console.log(chalk.blue("Danh sách sản phẩm:"), listProduct);
    }
})
// get detail product
yargs.command({
    command: "get-detail-product",
    builder: {
        id: {
            type: string
        },
    },
    handler: (args) => {
        const { id } = args;
        let listProduct = getListProduct();
        const product = listProduct.find((product) => product.id !== id);
        console.log(chalk.green("Sản phẩm:", product));
    }
});
//Thêm sản phẩm
yargs.command({
    command: "add-product",
    builder: {
        id: {type: string},
        name: { type: string },
        price: { type: number },
        amount: { type: number },
        description: {type: string}
    },
    handler: (args ) => {
        const { id, name, price, amount, description } = args;
        let listProduct = getListProduct();
        listProduct = [...listProduct, { id, name, price, amount, description }];
        updateListProduct(listProduct);
        console.log(chalk.red("Thêm thành công!"));
    }
});
// Xóa sản phẩm
yargs.command({
    command: "delete-product",
    builder: {
        id: { type: string }
    },
    handler: (args) => {
        const { id } = args;
        let listProduct = getListProduct();
        listProduct = listProduct.filter((product) => product.id !== id);
        updateListProduct(listProduct);
        console.log(chalk.red("Xóa thành công!"));
    }
})

// Sửa thông tin sản phẩm
yargs.command({
    command: "update-product",
    builder: {
        id: {type: string},
        name: { type: string },
        price: { type: number },
        amount: { type: number },
        description: {type: string}
    },
    handler: (agrs) => {
        const { id, name, price, amount, description } = agrs;
        let listProduct = getListProduct();
        const index = listProduct.findIndex((product) => product.id == id);
        if (index !== -1) {
            let product = listProduct[index];
            product = { ...product, name, price, amount, description };
            listProduct[index] = product;
            updateListProduct(listProduct);
            console.log(chalk.red("Sửa thành công!"));
        } else {
            console.log(chalk.red("Sửa không thành công!"));
        }
    }
})

yargs.parse();  