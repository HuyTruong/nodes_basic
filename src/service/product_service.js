const fs = require("fs");

// lấy danh sách sản phẩm tử file product.json
const getListProduct = () => {
    let listProduct = fs.readFileSync("./src/product.json");
    listProduct = JSON.parse(listProduct);
    return listProduct;
};
// cập nhât lại danh sách sản phẩm
const updateListProduct = (data) => {
    fs.writeFileSync("./src/product.json", JSON.stringify(data));
}


module.exports = {
    getListProduct,
    updateListProduct
};